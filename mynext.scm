(define-module (gnu packages mynext)
  #:use-modules (gnu packages web-browsers)
  #:use-modules (guix packages))

;; Latest from git :)
(define-public mynext
  (let ((dev "be23c2e3ff0c31d3164e0202223d6e19ec78fc92"))
    (package
     (inherit next-gtk-webkit)
     (version (string-take dev 7))
     (origin
       (commit dev)))))
